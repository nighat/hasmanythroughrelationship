<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CountryRequest;
//use App\Http\Controllers\AdminController AS Controller;
use App\Country;
use Session;
class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $countries = Country::with('users')->get();
        return view('country.index', compact('countries'));
    }

        public function users(Country $country)
    {
        $data = [
            'country' => $country
        ];
        return view('country.users', $data);
    }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
    {
        return view('country.create');
    }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(CountryRequest $request)
    {
        try {
            $data = $request->only('name');
            Country::create($data);
            Session::flash('message', 'Country added!');
            return redirect('country');
        } catch(Exception $e) {
            return redirect()->back()
                ->withErrors($e->getMessage())
                ->withInput();
        }
    }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  Profession $profession
         * @return \Illuminate\Http\Response
         */
        public function edit(Country $country)
    {
        return view('country.edit', compact('country'));
    }

        /**
         * Update the specified resource in storage.
         *
         * @param  ProfessionRequest $request
         * @param  Profession $profession
         * @return \Illuminate\Http\Response
         */
        public function update(CountryRequest $request, Country $country)
    {
        try {
            $data = $request->only('name');
            $country->update($data);
            Session::flash('message', 'Country updated!');
            return redirect('country');
        } catch(Exception $e) {
            return redirect()->back()
                ->withErrors($e->getMessage())
                ->withInput();
        }
    }

        /**
         * Remove the specified resource from storage.
         *
         * @param  Profession $profession
         * @return \Illuminate\Http\Response
         */
        public function destroy(country $country)
    {
        try {
            $name = $country->name;
            $country->delete();
            Session::flash('message', $name . ' deleted!');
            return redirect('country');
        } catch (Exception $e) {
            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }
    }
