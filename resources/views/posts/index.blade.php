@extends('layouts.app')

{{-- Page title --}}
@section('title', 'Post List')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Users <a href="{{ url('/posts/create') }}" class="btn btn-primary btn-xs"
                                            title="Add New User"><span
                            class="glyphicon glyphicon-plus" aria-hidden="true"/></a></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Post List
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        @include('layouts.alert')
                        <table class="table table-striped table-bordered table-hover" id="dataTables">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th> {{ trans('posts.name') }} </th>
                                <th> {{ trans('posts.title') }} </th>
                                <th> {{ trans('user.name') }} </th>
                                <th> {{ trans('user.country') }} </th>

                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($posts as $post)

                                <tr >
                                    <td>{{ $loop->index + 1}}</td>
                                    <td>{{ $post->name }}</td>
                                    <td>{{ $post->title }}</td>
                                    <td>{{ $post->user['name'] }}</td>
                                    <td>{{ $post->user->country['name'] }}</td>

                                    <td>
                                        <a href="{{ url('/posts/' . $post->id . '/edit') }}"
                                           class="btn btn-primary btn-xs"
                                           title="Edit User"><span class="glyphicon glyphicon-pencil"
                                                                  aria-hidden="true"/></a>
                                        <a href="{{ url('/posts/' . $post->id) }}"
                                           class="btn btn-primary btn-xs"
                                           title="View User"><span class="glyphicon glyphicon-eye-open"
                                                                   aria-hidden="true"/></a>


                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/posts', $post->id],
                                            'style' => 'display:inline'
                                        ]) !!}
                                        {{--@if($user->id !== Auth::user()->id)--}}
                                            {{--@php $disabled = '' @endphp--}}
                                        {{--@else--}}
                                            {{--@php $disabled = 'disabled' @endphp--}}
                                        {{--@endif--}}
                                        {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete User" />', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs',
                                               // $disabled,
                                                'onclick'=>'return confirm("Are you sure you want to delete ' . $post->name . '?")'
                                        ))!!}

                                        {!! Form::close() !!}


                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection

@push('css')
{{-- DataTables CSS --}}
<link href="{{ asset('sb-admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}"
      rel="stylesheet">
{{-- DataTables Responsive CSS --}}
<link href="{{ asset('sb-admin/bower_components/datatables-responsive/css/dataTables.responsive.css') }}"
      rel="stylesheet">
@endpush

@push('scripts')
{{-- DataTables JavaScript --}}
<script src="{{ asset('sb-admin/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('sb-admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#dataTables').DataTable({
            responsive: true
        });
    });
</script>
@endpush