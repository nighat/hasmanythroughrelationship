<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();

        return [
            'name' => 'filled|max:50',
            'title' => 'filled|max:50',
            'user_id' => 'filled|integer'
        ];
    }
    public function sanitize()
    {
        $this->merge(array_map('trim', $this->all()));

        $input = $this->all();

        $input['name'] = filter_var($input['name'], FILTER_SANITIZE_STRING);
        $input['title'] = filter_var($input['title'], FILTER_SANITIZE_STRING);
        $input['user_id'] = filter_var($input['user_id'], FILTER_VALIDATE_INT);

        $this->replace($input);
    }
}
