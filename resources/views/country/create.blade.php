@extends('layouts.app')

{{-- Page title --}}
@section('title', 'Create New Profession')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create New Country</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Profession Form
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => 'country', 'class' => 'form-horizontal']) !!}

                    @include('country.form')

                    {!! Form::close() !!}
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection