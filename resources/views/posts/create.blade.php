@extends('layouts.app')

{{-- Page title --}}
@section('title', 'Create New Post')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create New Post</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    User Form
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => '/posts', 'class' => 'form-horizontal']) !!}

                    @include('posts.form')

                    {!! Form::close() !!}
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection