<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class post extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'title','name',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
