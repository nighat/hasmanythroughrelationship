@extends('layouts.app')

{{-- Page title --}}
@section('title', 'Edit' . $country->name )

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit {{ $country->name }}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Country
                </div>
                <div class="panel-body">
                    {!! Form::open([
                    'method' => 'PATCH',
                    'url' => ['/country', $country->id],
                    'class' => 'form-horizontal'
                    ]) !!}

                    @include('country.form')

                    {!! Form::close() !!}
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection